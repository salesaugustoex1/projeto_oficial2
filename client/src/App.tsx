import React from 'react';
import './App.css';
import data from './data';

function App() {
     
const openMenu = () => {
      document.querySelector(".sidebar")?.classList.add("open")
}
 const closeMenu = () => {
  document.querySelector(".sidebar")?.classList.remove("open")
 }

  return (
    <div className="grid-container">
    <header className="header">
        <div className="brand">
            <button onClick= {openMenu}>
                &#9776;
            </button>
            <a href="index.html">Mercadinho</a>
        </div>
           <div className="header-link">
           <a href="cart.htlm">Carrinho</a>
          </div>
          </header>
           <aside className="sidebar">
               <h3>Mercadinho Categorias</h3>
               <button className="sidebar-close-button" onClick={closeMenu}>x</button> 
              <ul>
                  <li>
                      <a href="index.html">Alimentos</a>
                  </li>
              </ul>
           </aside>
              <main className="main">
                  <div className="content">
                  <ul className="produtos">

                    {
                      data.produtos.map( produto => 
                        <li>
                      <div className="produto">
                          <img className="produto-imagem" src={produto.imagem} alt="produto"/> 
                          <div className= "produto-nome">
                              <a href="produto-html">{produto.nome}</a>
                          </div>
                          <div className="produto-marca">{produto.marca}</div>    
                          <div className="produto-preco">${produto.preco}</div>
            
                      </div>
                  </li>)                         
                    }                   
                 </ul>
              </div>

           </main>
           <footer className="footer">
               O mais barato é aqui !
           </footer>

  </div>
  );
}

export default App;
